# [Project] Cashiera Mobile

A freelance project to help local business managing their business using application (web for management and mobile for smart cashier / selling product).

## References

Web App: [https://gitlab.com/redevartk/cashiera-web](https://gitlab.com/redevartk/cashiera-web)

## Properties

* Flutter SDK: >=2.12.0 <3.0.0

## Installation

1. Clone repo
2. Install Android Studio
3. Install flutter SDK / Plugin
4. Open the project
5. Configure the apps by flutter guide: [https://docs.flutter.dev/get-started/install](https://docs.flutter.dev/get-started/install)
6. Configure the apps according to Cashiera Web App Url in "public.dart"
7. Connect your android or use web as main running app
8. Test the apps

## Developer Note

1. This maybe a lengthy in order to config the apps because it's using old flutter version, and old runtime
